CREATE TABLE accounts(
	id SERIAL NOT NULL,
	login VARCHAR(100) UNIQUE NOT NULL,
	pwd VARCHAR(40) NOT NULL, -- Size of a blowfish hash
	is_admin BOOLEAN DEFAULT FALSE NOT NULL,
	CONSTRAINT pk_accounts PRIMARY KEY (id)
);

CREATE TABLE categories(
	id SERIAL NOT NULL,
	name VARCHAR(200) NOT NULL,
	CONSTRAINT pk_categories PRIMARY KEY (id)
);

CREATE TABLE calendars(
	id SERIAL NOT NULL,
	name VARCHAR(200) NOT NULL,
	is_activated BOOLEAN DEFAULT TRUE NOT NULL,
	account_id INTEGER NOT NULL,
	category_id INTEGER NOT NULL,
	CONSTRAINT pk_calendars PRIMARY KEY (id),
	CONSTRAINT fk_calendars_account FOREIGN KEY (account_id) REFERENCES accounts(id),
	CONSTRAINT fk_calendars_categories FOREIGN KEY (category_id) REFERENCES categories(id)
);

CREATE TABLE frequency_types (
	id SERIAL NOT NULL,
	name VARCHAR(100) NOT NULL,
	CONSTRAINT pk_frequency_types PRIMARY KEY (id)
);

CREATE TABLE week_days (
	id SERIAL NOT NULL,
	name VARCHAR(50) NOT NULL,
	CONSTRAINT pk_week_days PRIMARY KEY (id)
);

CREATE TABLE events(
	id SERIAL NOT NULL,
	name VARCHAR(200) NOT NULL,
	summary VARCHAR(500),
	place VARCHAR(200),
	date_begin DATE NOT NULL,
	is_all_day BOOLEAN DEFAULT FALSE NOT NULL,
	date_end DATE DEFAULT NULL,
	hour_begin TIME DEFAULT NULL,
	hour_end TIME DEFAULT NULL,
	interv SMALLINT DEFAULT 0,
	calendar_id INTEGER NOT NULL,
	modified TIMESTAMP DEFAULT NULL,
	frequency_type_id INTEGER DEFAULT NULL,
	CONSTRAINT pk_events PRIMARY KEY (id),
	CONSTRAINT fk_events_calendars FOREIGN KEY (calendar_id) REFERENCES calendars(id),
	CONSTRAINT fk_events_frequency_types FOREIGN KEY (frequency_type_id) REFERENCES frequency_types(id)
);

CREATE TABLE events_week_days(
	id SERIAL NOT NULL,
	event_id INTEGER NOT NULL,
	week_day_id INTEGER NOT NULL,
	CONSTRAINT pk_events_week_days PRIMARY KEY (id),
	CONSTRAINT fk_events_week_days_frequency_types FOREIGN KEY (event_id) REFERENCES events(id),
	CONSTRAINT fk_events_week_days_week_days FOREIGN KEY (week_day_id) REFERENCES week_days(id)
);

CREATE TABLE i18n (
	id SERIAL NOT NULL,
	locale VARCHAR(6) NOT NULL,
	model VARCHAR(255) NOT NULL,
	foreign_key INTEGER NOT NULL,
	field VARCHAR(255) NOT NULL,
	content text,
	CONSTRAINT pk_i18n PRIMARY KEY (id)
);

-- Initial accounts
INSERT INTO accounts (login, pwd, is_admin) VALUES ('dev', '422beeaa286f53370e5ad34dea8d67bfee32b8a8', true);
INSERT INTO accounts (login, pwd, is_admin) VALUES ('test', 'c00a6e1fd355b1aa0d763437a7744403750b4a0c', false);

-- Week days in application's main language
INSERT INTO week_days (name) VALUES ('Monday');
INSERT INTO week_days (name) VALUES ('Thuesday');
INSERT INTO week_days (name) VALUES ('Wednesday');
INSERT INTO week_days (name) VALUES ('Thursday');
INSERT INTO week_days (name) VALUES ('Friday');
INSERT INTO week_days (name) VALUES ('Saturday');
INSERT INTO week_days (name) VALUES ('Sunday');

INSERT INTO frequency_types (name) VALUES ('every day');
INSERT INTO frequency_types (name) VALUES ('every week');
INSERT INTO frequency_types (name) VALUES ('same day each month (ex : 01/20, 02/20, ...)');
INSERT INTO frequency_types (name) VALUES ('every x days of the month (ex : every fourth thursday)');
INSERT INTO frequency_types (name) VALUES ('every year');

-- Reference every translation in db
INSERT INTO i18n (locale, model, foreign_key, field, content) VALUES ('fra', 'WeekDay', 1, 'name', 'Lundi');
INSERT INTO i18n (locale, model, foreign_key, field, content) VALUES ('fra', 'WeekDay', 2, 'name', 'Mardi');
INSERT INTO i18n (locale, model, foreign_key, field, content) VALUES ('fra', 'WeekDay', 3, 'name', 'Mercredi');
INSERT INTO i18n (locale, model, foreign_key, field, content) VALUES ('fra', 'WeekDay', 4, 'name', 'Jeudi');
INSERT INTO i18n (locale, model, foreign_key, field, content) VALUES ('fra', 'WeekDay', 5, 'name', 'Vendredi');
INSERT INTO i18n (locale, model, foreign_key, field, content) VALUES ('fra', 'WeekDay', 6, 'name', 'Samedi');
INSERT INTO i18n (locale, model, foreign_key, field, content) VALUES ('fra', 'WeekDay', 7, 'name', 'Dimanche');

INSERT INTO i18n (locale, model, foreign_key, field, content) VALUES ('fra', 'FrequencyType', 1, 'name', 'tous les jours');
INSERT INTO i18n (locale, model, foreign_key, field, content) VALUES ('fra', 'FrequencyType', 2, 'name', 'toutes les semaines');
INSERT INTO i18n (locale, model, foreign_key, field, content) VALUES ('fra', 'FrequencyType', 3, 'name', 'tous les mois le même jour (ex : 20/01, 20/02, ...)');
INSERT INTO i18n (locale, model, foreign_key, field, content) VALUES ('fra', 'FrequencyType', 4, 'name', 'tous les x du mois (ex : tous les quatrièmes jeudis)');
INSERT INTO i18n (locale, model, foreign_key, field, content) VALUES ('fra', 'FrequencyType', 5, 'name', 'tous les ans');

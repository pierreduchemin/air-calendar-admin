<?php
/**
 * Bake Template for Controller action generation.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.actions
 * @since         CakePHP(tm) v 1.3
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>

/**
 * <?= $admin ?>index method
 *
 * @return void
 */
	public function <?= $admin ?>index() {
		$this-><?= $currentModelName ?>->recursive = 0;
		$this->set('<?= $pluralName ?>', $this->paginate());
	}

/**
 * <?= $admin ?>view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function <?= $admin ?>view($id = null) {
		if (!$this-><?= $currentModelName; ?>->exists($id)) {
			throw new NotFoundException(__('Invalid <?= strtolower($singularHumanName); ?>'));
		}
		$options = array('conditions' => array('<?= $currentModelName; ?>.' . $this-><?= $currentModelName; ?>->primaryKey => $id));
		$this->set('<?= $singularName; ?>', $this-><?= $currentModelName; ?>->find('first', $options));
	}

<?php $compact = array(); ?>
/**
 * <?= $admin ?>add method
 *
 * @return void
 */
	public function <?= $admin ?>add() {
		if ($this->request->is('post')) {
			$this-><?= $currentModelName; ?>->create();
			if ($this-><?= $currentModelName; ?>->save($this->request->data)) {
<?php if ($wannaUseSession): ?>
				$this->Session->setFlash(__('The <?= strtolower($singularHumanName); ?> has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
<?php else: ?>
				$this->flash(__('<?= ucfirst(strtolower($currentModelName)); ?> saved.'), array('action' => 'index'));
<?php endif; ?>
			} else {
<?php if ($wannaUseSession): ?>
				$this->Session->setFlash(__('The <?= strtolower($singularHumanName); ?> could not be saved. Please, try again.'), 'flash/error');
<?php endif; ?>
			}
		}
<?php
	foreach (array('belongsTo', 'hasAndBelongsToMany') as $assoc):
		foreach ($modelObj->{$assoc} as $associationName => $relation):
			if (!empty($associationName)):
				$otherModelName = $this->_modelName($associationName);
				$otherPluralName = $this->_pluralName($associationName);
				echo "\t\t\${$otherPluralName} = \$this->{$currentModelName}->{$otherModelName}->find('list');\n";
				$compact[] = "'{$otherPluralName}'";
			endif;
		endforeach;
	endforeach;
	if (!empty($compact)):
		echo "\t\t\$this->set(compact(".join(', ', $compact)."));\n";
	endif;
?>
	}

<?php $compact = array(); ?>
/**
 * <?= $admin ?>edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function <?= $admin; ?>edit($id = null) {
        $this-><?= $currentModelName; ?>->id = $id;
		if (!$this-><?= $currentModelName; ?>->exists($id)) {
			throw new NotFoundException(__('Invalid <?= strtolower($singularHumanName); ?>'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this-><?= $currentModelName; ?>->save($this->request->data)) {
<?php if ($wannaUseSession): ?>
				$this->Session->setFlash(__('The <?= strtolower($singularHumanName); ?> has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
<?php else: ?>
				$this->flash(__('The <?= strtolower($singularHumanName); ?> has been saved.'), array('action' => 'index'));
<?php endif; ?>
			} else {
<?php if ($wannaUseSession): ?>
				$this->Session->setFlash(__('The <?= strtolower($singularHumanName); ?> could not be saved. Please, try again.'), 'flash/error');
<?php endif; ?>
			}
		} else {
			$options = array('conditions' => array('<?= $currentModelName; ?>.' . $this-><?= $currentModelName; ?>->primaryKey => $id));
			$this->request->data = $this-><?= $currentModelName; ?>->find('first', $options);
		}
<?php
		foreach (array('belongsTo', 'hasAndBelongsToMany') as $assoc):
			foreach ($modelObj->{$assoc} as $associationName => $relation):
				if (!empty($associationName)):
					$otherModelName = $this->_modelName($associationName);
					$otherPluralName = $this->_pluralName($associationName);
					echo "\t\t\${$otherPluralName} = \$this->{$currentModelName}->{$otherModelName}->find('list');\n";
					$compact[] = "'{$otherPluralName}'";
				endif;
			endforeach;
		endforeach;
		if (!empty($compact)):
			echo "\t\t\$this->set(compact(".join(', ', $compact)."));\n";
		endif;
	?>
	}

/**
 * <?= $admin ?>delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function <?= $admin; ?>delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this-><?= $currentModelName; ?>->id = $id;
		if (!$this-><?= $currentModelName; ?>->exists()) {
			throw new NotFoundException(__('Invalid <?= strtolower($singularHumanName); ?>'));
		}
		if ($this-><?= $currentModelName; ?>->delete()) {
<?php if ($wannaUseSession): ?>
			$this->Session->setFlash(__('<?= ucfirst(strtolower($singularHumanName)); ?> deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
<?php else: ?>
			$this->flash(__('<?= ucfirst(strtolower($singularHumanName)); ?> deleted'), array('action' => 'index'));
<?php endif; ?>
		}
<?php if ($wannaUseSession): ?>
		$this->Session->setFlash(__('<?= ucfirst(strtolower($singularHumanName)); ?> was not deleted'), 'flash/error');
<?php else: ?>
		$this->flash(__('<?= ucfirst(strtolower($singularHumanName)); ?> was not deleted'), array('action' => 'index'));
<?php endif; ?>
		$this->redirect(array('action' => 'index'));
	}
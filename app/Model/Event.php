<?php
App::uses('AppModel', 'Model');
/**
 * Event Model
 *
 * @property Calendar $Calendar
 * @property Category $Category
 * @property FrequencyType $FrequencyType
 */
class Event extends AppModel {

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'name';

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate;

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
			'Calendar' => array(
					'className' => 'Calendar',
					'foreignKey' => 'calendar_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			),
			'FrequencyType' => array(
					'className' => 'FrequencyType',
					'foreignKey' => 'frequency_type_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			),
	);	
		
	/**
	 * hasAndBelongsToMany associations
	 *
	 * @var array
	 */
	public $hasAndBelongsToMany = array(
			'WeekDay' => array(
					'className' => 'WeekDay',
					'joinTable' => 'events_week_days',
					'foreignKey' => 'event_id',
					'associationForeignKey' => 'week_day_id',
					'unique' => 'keepExisting',
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'finderQuery' => '',
			)
	);

	public function __construct() {
		parent::__construct();
		$this->validate = array(
				'id' => array(
						'notEmpty' => array(
								'rule' => array('notEmpty'),
						),
						'naturalNumber' => array(
								'rule'    => array('naturalNumber', true),
								'message' => __('You have to provide a positive numeric value'),
						),
				),
				'name' => array(
						'notEmpty' => array(
								'rule' => array('notEmpty'),
						),
						'maxLenght' => array(
								'rule' => array('maxLength', 200),
								'message' => __('This name is too long'),
						),
				),
				'summary' => array(
						'maxLenght' => array(
								'rule' => array('maxLength', 500),
								'message' => __('This summary is too long'),
								'allowEmpty' => true,
						),
				),
				'place' => array(
						'maxLenght' => array(
								'rule' => array('maxLength', 200),
								'message' => __('This name is too long'),
								'allowEmpty' => true,
						),
				),
				'date_begin' => array(
						'notEmpty' => array(
								'rule' => array('notEmpty'),
						),
						'date' => array(
								'rule' => array('date'),
								'message' => __('You have to provide a date'),
						),
				),
				'hour_begin' => array(
						'time' => array(
								'rule' => array('time'),
								'message' => __('You have to provide a valid hour'),
								'allowEmpty' => true,
						),
				),
				'is_all_day' => array(
						'notEmpty' => array(
								'rule' => array('notEmpty'),
						),
						'boolean' => array(
								'rule' => array('boolean'),
								'message' => __('You can\'t set a value other than "true" or "false"'),
						),
				),
				'date_end' => array(
						'date' => array(
								'rule' => array('date'),
								'message' => __('You have to provide a date'),
								'allowEmpty' => true,
						),
				),
				'hour_end' => array(
						'time' => array(
								'rule' => array('time'),
								'message' => __('You have to provide a valid hour'),
								'allowEmpty' => true,
						),
				),
				'interv' => array(
						'naturalNumber' => array(
								'rule'    => array('naturalNumber', true),
								'message' => __('You have to provide a positive integer value'),
								'allowEmpty' => true,
						),
						'range' => array(
								'rule'    => array('range', -1, 32768),
								'message' => __('Please enter a number between 0 and 32767'),
						),
				),
				'calendar_id' => array(
						'notEmpty' => array(
								'rule' => array('notEmpty'),
						),
						'naturalNumber' => array(
								'rule'    => array('naturalNumber', true),
								'message' => __('You have to provide a positive numeric value'),
						),
				),
		);
	}
}

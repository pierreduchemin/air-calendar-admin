<?php
App::uses('AppModel', 'Model');
/**
 * Calendar Model
 *
 * @property Account $Account
 * @property Event $Event
 */
class Calendar extends AppModel {

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'name';

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate;

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
			'Account' => array(
					'className' => 'Account',
					'foreignKey' => 'account_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			),
			'Category' => array(
					'className' => 'Category',
					'foreignKey' => 'category_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			)
	);

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
			'Event' => array(
					'className' => 'Event',
					'foreignKey' => 'calendar_id',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			)
	);

	public function __construct() {
		parent::__construct();
		$this->validate = array(
				'id' => array(
						'notEmpty' => array(
								'rule' => array('notEmpty'),
						),
						'numeric' => array(
								'rule' => array('numeric'),
								'message' => __('You have to provide a numeric value'),
						),
				),
				'name' => array(
						'notEmpty' => array(
								'rule' => array('notEmpty'),
						),
						'maxLenght' => array(
								'rule' => array('maxLength', 200),
								'message' => __('This name is too long'),
						),
				),
				'is_activated' => array(
						'notEmpty' => array(
								'rule' => array('notEmpty'),
						),
						'boolean' => array(
								'rule' => array('boolean'),
								'message' => __('You can\'t set a value other than "true" or "false"'),
						),
				),
				'account_id' => array(
						'notEmpty' => array(
								'rule' => array('notEmpty'),
						),
						'naturalNumber' => array(
								'rule'    => array('naturalNumber', true),
								'message' => __('You have to provide a positive numeric value'),
						),
				),
				'category_id' => array(
						'notEmpty' => array(
								'rule' => array('notEmpty'),
						),
						'naturalNumber' => array(
								'rule'    => array('naturalNumber', true),
								'message' => __('You have to provide a positive numeric value'),
						),
				),
		);
	}
}

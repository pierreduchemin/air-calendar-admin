<?php
App::uses('AppModel', 'Model');
/**
 * FrequencyType Model
 *
 * @property Event $Event
 * @property WeekDay $WeekDay
 */
class FrequencyType extends AppModel {

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'name';

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate;

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
			'Event' => array(
					'className' => 'Event',
					'foreignKey' => 'frequency_type_id',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			)
	);

	/**
	 * hasAndBelongsToMany associations
	 *
	 * @var array
	 */
	public $hasAndBelongsToMany = array(
			'WeekDay' => array(
					'className' => 'WeekDay',
					'joinTable' => 'frequency_types_week_days',
					'foreignKey' => 'frequency_type_id',
					'associationForeignKey' => 'week_day_id',
					'unique' => 'keepExisting',
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'finderQuery' => '',
			)
	);

	public $actsAs = array(
        'Translate' => array(
            'name'
        )
    );

	public function __construct() {
		parent::__construct();
		$this->validate = array(
				'id' => array(
						'notEmpty' => array(
								'rule' => array('notEmpty'),
						),
						'naturalNumber' => array(
								'rule'    => array('naturalNumber', true),
								'message' => __('You have to provide a positive numeric value'),
						),
				),
				'name' => array(
						'notEmpty' => array(
								'rule' => array('notEmpty'),
						),
						'maxLenght' => array(
								'rule' => array('maxLength', 100),
								'message' => __('This name is too long'),
						),
				),
		);
	}
}

<?php
App::uses('AppModel', 'Model');
/**
 * WeekDay Model
 *
 * @property FrequencyType $FrequencyType
 */
class WeekDay extends AppModel {

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'name';

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate;

	/**
	 * hasAndBelongsToMany associations
	 *
	 * @var array
	 */
	public $hasAndBelongsToMany = array(
			'Event' => array(
					'className' => 'Event',
					'joinTable' => 'events_week_days',
					'foreignKey' => 'week_day_id',
					'associationForeignKey' => 'event_id',
					'unique' => 'keepExisting',
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'finderQuery' => '',
			)
	);

	public $actsAs = array(
        'Translate' => array(
            'name'
        )
    );

	public function __construct() {
		parent::__construct();
		$this->validate = array(
				'id' => array(
						'notEmpty' => array(
								'rule' => array('notEmpty'),
						),
						'naturalNumber' => array(
								'rule'    => array('naturalNumber', true),
								'message' => __('You have to provide a positive numeric value'),
						),
				),
				'name' => array(
						'notEmpty' => array(
								'rule' => array('notEmpty'),
						),
						'alphaNumeric' => array(
								'rule' => array('alphaNumeric'),
								'message' => __('This field can just contain letters and figures'),
						),
						'maxLenght' => array(
								'rule' => array('maxLength', 50),
								'message' => __('This name is too long'),
						),
				),
		);
	}
}

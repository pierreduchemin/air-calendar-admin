<?php
App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

/**
 * Account Model
 *
 * @property Calendar $Calendar
 */
class Account extends AppModel {

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'login';

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate;

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
			'Calendar' => array(
					'className' => 'Calendar',
					'foreignKey' => 'account_id',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			)
	);

	public function __construct() {
		parent::__construct();
		$this->validate = array(
				'id' => array(
						'notEmpty' => array(
								'rule' => array('notEmpty'),
						),
						'naturalNumber' => array(
								'rule'    => array('naturalNumber', true),
								'message' => __('You have to provide a positive numeric value'),
						),
				),
				'login' => array(
						'notEmpty' => array(
								'rule' => array('notEmpty'),
						),
						'alphaNumeric' => array(
								'rule' => array('alphaNumeric'),
								'message' => __('This field can just contain letters and figures'),
						),
						'unique' => array(
								'rule' => 'isUnique',
								'message' => array(__('This username has already been taken'), true)
						),
						'minLenght' => array(
								'rule' => array('minLength', 6),
								'message' => __('This login is too short'),
						),
						'maxLenght' => array(
								'rule' => array('maxLength', 100),
								'message' => __('This login is too long'),
						),
				),
				'pwd' => array(
						'notEmpty' => array(
								'rule' => array('notEmpty'),
						),
						'minLenght' => array(
								'rule' => array('minLength', 6),
								'message' => __('This password is too short'),
						),
						'maxLenght' => array(
								'rule' => array('maxLength', 40),
								'message' => __('This password is too long'),
						),
				),
				'is_admin' => array(
						'notEmpty' => array(
								'rule' => array('notEmpty'),
						),
						'boolean' => array(
								'rule' => array('boolean'),
								'message' => __('You can\'t set a value other than "true" or "false"'),
						),
				),
		);
	}

	public function beforeSave($options = array()) {
		$haveToHash = false;

		// If we are editing data
		if (isset($this->data['Account']['id']) && $this->exists($this->data['Account']['id'])) {
			$dbAccount = $this->findById($this->data['Account']['id']);
			$haveToHash = $dbAccount['Account']['pwd'] != $this->data['Account']['pwd'];
		} else {
			$haveToHash = true;
		}
		if ($haveToHash) {
			$passwordHasher = new SimplePasswordHasher();
			$this->data['Account']['pwd'] = $passwordHasher->hash(
					$this->data['Account']['pwd']
			);
		}
		return true;
	}
}

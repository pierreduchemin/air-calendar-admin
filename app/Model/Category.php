<?php
App::uses('AppModel', 'Model');
/**
 * Category Model
 *
 * @property Event $Event
 */
class Category extends AppModel {

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'name';

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate;

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
			'Calendar' => array(
					'className' => 'Calendar',
					'foreignKey' => 'category_id',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			)
	);

	public function __construct() {
		parent::__construct();
		$this->validate = array(
				'id' => array(
						'notEmpty' => array(
								'rule' => array('notEmpty'),
						),
						'naturalNumber' => array(
								'rule'    => array('naturalNumber', true),
								'message' => __('You have to provide a positive numeric value'),
						),
				),
				'name' => array(
						'notEmpty' => array(
								'rule' => array('notEmpty'),
						),
						'maxLenght' => array(
								'rule' => array('maxLength', 200),
								'message' => __('This name is too long'),
						),
				),
		);
	}
}

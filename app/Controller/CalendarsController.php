<?php
App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');

/**
 * Calendars Controller
 *
 * @property Calendar $Calendar
 * @property PaginatorComponent $Paginator
 */
class CalendarsController extends AppController {
	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array('Auth', 'Paginator');
	
	public function beforeFilter() {
		$this->Auth->allow('export');
	}

	/**
	 * admin_index method
	 *
	 * @return void
	 */
	public function admin_index() {
		$this->Calendar->recursive = 0;
		$currentAccount = $this->Session->read('Auth.User');
		$this->set('calendars', $this->paginate(null, array('Account.id' => $currentAccount['Account']['id'])));
	}

	/**
	 * admin_view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_view($id = null) {
		if (!$this->Calendar->exists($id)) {
			throw new NotFoundException(__('Invalid calendar'));
		}
		$accounts = $this->Session->read('Auth.User');
		$currentCalendar = $this->Calendar->findById($id);
		if($currentCalendar['Account']['id'] != $accounts['Account']['id']) {
			$this->Session->setFlash(__('You are not allowed to view this calendar'), 'flash/error');
			throw new ForbiddenException();
		}
		$options = array('conditions' => array('Calendar.' . $this->Calendar->primaryKey => $id));
		$this->set('calendar', $this->Calendar->find('first', $options));
	}

	/**
	 * admin_add method
	 *
	 * @return void
	 */
	public function admin_add() {
		$accounts = $this->Session->read('Auth.User');
		if ($this->request->is('post')) {
			$this->Calendar->create();
			$this->request->data['Calendar']['account_id'] = $accounts['Account']['id'];
			if ($this->Calendar->save($this->request->data)) {
				if ($this->request->data['Calendar']['is_activated']) {
					$this->Session->setFlash(__('The calendar has been saved') . '. ' .
							__('The broadcast adress of your new calendar is %s',
									$_SERVER['HTTP_HOST'] . DS . basename(dirname(APP)) . DS . 'calendars' . DS . 'export' . DS .  $this->Calendar->getLastInsertID())
							, 'flash/success');
				} else {
					$this->Session->setFlash(__('The calendar has been saved') . '. ' . ('You have to activate it to make it available.'), 'flash/success');
				}
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The calendar could not be saved'), 'flash/error');
			}
		}
		$accounts = array($accounts['Account']['id'] => $accounts['Account']['login']);
		$categories = $this->Calendar->Category->find('list');
		if (count($categories) === 0) {
			$this->Session->setFlash(__('You have to add a category first'), 'flash/warning');
		}
		$this->set(compact('accounts', 'categories'));
	}

	/**
	 * admin_edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_edit($id = null) {
		$this->Calendar->id = $id;
		if (!$this->Calendar->exists($id)) {
			throw new NotFoundException(__('Invalid calendar'));
		}
		$currentCalendar = $this->Calendar->findById($id);
		if ($this->request->is('post') || $this->request->is('put')) {
			$currentAccount = $this->Session->read('Auth.User');
			if ($currentCalendar['Account']['id'] == $currentAccount['Account']['id']) {
				if ($this->Calendar->save($this->request->data)) {
					if ($this->request->data['Calendar']['is_activated']) {
						$this->Session->setFlash(__('The calendar has been saved') . '. ' .
								__('The broadcast adress of your new calendar is %s',
										$_SERVER['HTTP_HOST'] . Configure::read('app_path') . DS . 'calendars' . DS . 'export' . DS .  $id)
								, 'flash/success');
					} else {
						$this->Session->setFlash(__('The calendar has been saved') . '. ' . ('You have to activate it to make it available.'), 'flash/success');
					}
				} else {
					$this->Session->setFlash(__('The calendar could not be saved'), 'flash/error');
				}
			} else {
				$this->Session->setFlash(__('You are not allowed to edit this calendar'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Calendar.' . $this->Calendar->primaryKey => $id));
			$this->request->data = $this->Calendar->find('first', $options);
		}
		$accounts = $this->Session->read('Auth.User');
		if($currentCalendar['Account']['id'] != $accounts['Account']['id']) {
			$this->Session->setFlash(__('You are not allowed to edit this calendar'), 'flash/error');
			throw new ForbiddenException();
		}
		$categories = $this->Calendar->Category->find('list');
		$this->set(compact('categories', 'id'));
	}

	/**
	 * admin_delete method
	 *
	 * @throws NotFoundException
	 * @throws MethodNotAllowedException
	 * @param string $id
	 * @return void
	 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Calendar->id = $id;
		if (!$this->Calendar->exists()) {
			throw new NotFoundException(__('Invalid calendar'));
		}
		$currentCalendar = $this->Calendar->findById($id);
		$currentAccount = $this->Session->read('Auth.User');
		if ($currentCalendar['Account']['id'] == $currentAccount['Account']['id']) {
				
			// Always true : a verification have to be done after this
			$this->Calendar->Event->deleteAll(array('Event.calendar_id' => $currentCalendar['Calendar']['id']));
			$eventNb = $this->Calendar->Event->find('count', array('conditions' => array('Event.calendar_id' => $currentCalendar['Calendar']['id'])));
				
			if ($eventNb == 0) {
				if ($this->Calendar->delete()) {
					$this->Session->setFlash(__('Calendar deleted'), 'flash/success');
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('Calendar was not deleted'), 'flash/error');
				}
			}
		} else {
			$this->Session->setFlash(__('You are not allowed to delete this calendar'), 'flash/error');
		}
		$this->redirect(array('action' => 'index'));
	}

	public function export($id = null) {
		$this->Calendar->id = $id;
		if (!$this->Calendar->exists()) {
			throw new NotFoundException(__('Invalid calendar'));
		}
		$currentCalendar = $this->Calendar->findById($id);
		if ($currentCalendar['Calendar']['is_activated']) {
			$sanCalName = Sanitize::paranoid($currentCalendar['Calendar']['name']);
			$filename = $sanCalName . '_' . time() . '.ics';
			$fullPath = Configure::read('path_out_ical') . $filename;
			
			// Recherche du fichier
			$dir = opendir(Configure::read('path_out_ical'));
			$exists = false;
			while (($file = readdir($dir)) !== false && !$exists) {
				if (strpos($file, '_') !== false) {
					$filenameSearch = preg_split('/_/', $file, NULL);
					if($file != '.' && $file != '..' && !is_dir($dir . $file) && $filenameSearch[0] == $sanCalName) {
						$exists = true;
						$fullPath = Configure::read('path_out_ical') . $file;
						$filename = $file;
					}
				}
			}
			closedir($dir);
						
			if ($exists) {
				$lastModificationTime = $this->Calendar->Event->find('first', array(
							'fields' => array('max(modified) AS lastmodification'),
							'conditions' => array('calendar_id' => $id)
						)
				);
				
				$filenameSearch = pathinfo($filenameSearch[1]);
				if (strtotime($lastModificationTime[0]['lastmodification']) != $filenameSearch['filename']) {
					$this->generateIcal($sanCalName, $fullPath, $id);
				}
			} else {
				$this->generateIcal($sanCalName, $fullPath, $id);
			}
			
			// Send file. From http://php.developpez.com/faq/?page=fichiers_upload#fichiers_forcedownload
			ini_set('zlib.output_compression', 0);

			header('Pragma: public');
			header('Cache-Control: must-revalidate, pre-check=0, post-check=0, max-age=0');

			header('Content-Tranfer-Encoding: none');
			header('Content-Length: ' . filesize($fullPath));
			header('Content-MD5: ' . base64_encode(md5_file($fullPath)));
			header('Content-Type: application/octetstream; name="' . $filename . '"');
			header('Content-Disposition: attachment; filename="' . $filename . '"');

			header('Date: ' . gmdate(DATE_RFC1123));
			header('Expires: ' . gmdate(DATE_RFC1123, time() + 1)); // Laisse 1 seconde pour réaliser l'envoi du fichier
			header('Last-Modified: ' . gmdate(DATE_RFC1123, filemtime($fullPath)));

			readfile($fullPath);
			
			// Required to avoid sending corrupted files
			exit;
		} else {
			$this->Session->setFlash(__('This calendar is not activated'), 'flash/error');
			throw new ForbiddenException();
		}
	}
	
	private function generateIcal($sanCalName, $fullPath, $calId) {
		$file = new File($fullPath, true, 0644);
		$file->safe(basename($fullPath), 'ics');
			
		// See http://www.ietf.org/rfc/rfc2445.txt for more details on RFC2445
		$content = "BEGIN:VCALENDAR\nVERSION:2.0\nPRODID:-//" .
				Sanitize::paranoid($_SERVER['HTTP_HOST'] . DS . basename(dirname(APP))) .
				"//NONSGML Air-Calendar v1.0//EN";
			
		// Default values, but if the sandard changes, it will already be set
		$content .= "\nCALSCALE:GREGORIAN\nMETHOD:PUBLISH";
		
		$events = $this->Calendar->Event->find('all', array(
				'conditions' => array(
						'Calendar.id' => $calId
				)
		));
		foreach ($events as $e) {
			$d = new DateTime($e['Event']['date_begin']);
			$dtstart = "\nDTSTART:" . $d->format('Ymd') . 'T' . substr($e['Event']['hour_begin'], 0, 2) . substr($e['Event']['hour_begin'], 3, 2) . '00Z';
			if (isset($e['Event']['date_end'])) {
				$d = new DateTime($e['Event']['date_end']);
				$dtend = "\nDTEND:" . $d->format('Ymd') . 'T' . substr($e['Event']['hour_end'], 0, 2) . substr($e['Event']['hour_end'], 3, 2) . '00Z';
			} else {
				$dtend = '';
			}
		
			$rrule = '';
			$interv = '';
			if ($e['Event']['interv'] > 0) {
				$interv = ';INTERVAL=' . $e['Event']['interv'];
			}
			if ($e['Event']['frequency_type_id'] == 1) {
				$rrule = "\nRRULE:FREQ=DAILY" . $interv;
			}
			if ($e['Event']['frequency_type_id'] == 2) {
				$rrule = "\nRRULE:FREQ=WEEKLY" . $interv;
			}
			if ($e['Event']['frequency_type_id'] == 3) {
				$rrule = "\nRRULE:FREQ=MONTHLY" . $interv;
			}
			if ($e['Event']['frequency_type_id'] == 4) {
				$rrule = "\nRRULE:FREQ=MONTHLY" . $interv;
			}
			if ($e['Event']['frequency_type_id'] == 5) {
				$rrule = "\nRRULE:FREQ=YEARLY" . $interv;
			}
		
			$content .= "\nBEGIN:VEVENT" .
					$dtstart . $dtend . $rrule . "\nSUMMARY:" . $e['Event']['summary'] .
					"\nLOCATION:" . $e['Event']['place'] . "\nEND:VEVENT";
		}
		$content .= "\nEND:VCALENDAR\n";
		$file->write($content);
		$file->close();
	}
}

<?php
App::uses('AppController', 'Controller');
/**
 * Events Controller
 *
 * @property Event $Event
 * @property PaginatorComponent $Paginator
 */
class EventsController extends AppController {

	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array('Paginator', 'RequestHandler');

	/**
	 * admin_add method
	 *
	 * @return void
	 */
	public function admin_add($id = null) {
		$currentAccount = $this->Session->read('Auth.User');
		if ($this->request->is('post')) {
			$currentCalendar = $this->Event->Calendar->findById($id);
			if ($currentCalendar['Calendar']['account_id'] == $currentAccount['Account']['id']) {
				$this->Event->create();
				$newEvent = $this->request->data;
				if ($newEvent['Event']['frequency_type_id'] == ''
						 && $newEvent['Event']['date_end']['month'] == '' && $newEvent['Event']['date_end']['day'] == '' && $newEvent['Event']['date_end']['year'] == ''
						 && $newEvent['Event']['hour_end']['hour'] == '' && $newEvent['Event']['hour_end']['min'] == '') {
					$this->Session->setFlash(__('You have to specify end date and end hour if event is not recurring.'), 'flash/error');
				} else {
					$newEvent['Event']['calendar_id'] = $id;
					if ($newEvent['Event']['frequency_type_id'] != '' && $newEvent['Event']['interv'] == '') {
						$newEvent['Event']['interv'] = '1';
					}
					if ($this->Event->save($newEvent)) {
						$this->Session->setFlash(__('The event has been saved'), 'flash/success');
						return $this->redirect(array('controller' => 'calendars', 'action' => 'edit', $newEvent['Event']['calendar_id']));
					} else {
						$this->Session->setFlash(__('The event could not be saved'), 'flash/error');
					}
				}
			} else {
				$this->Session->setFlash(__('You are not allowed to edit this calendar'), 'flash/error');
			}
		}
		$frequencyTypes = $this->Event->FrequencyType->find('list');
		$weekDays = $this->Event->FrequencyType->WeekDay->find('list');
		$this->set(compact('frequencyTypes', 'weekDays'));
	}

	/**
	 * admin_edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_edit($id = null) {
		if (!$this->Event->exists($id)) {
			throw new NotFoundException(__('Invalid event'));
		}
		$currentAccount = $this->Session->read('Auth.User');
		$currentEvent = $this->Event->findById($id);
		$calendarId = $currentEvent['Event']['calendar_id'];
		if ($this->request->is(array('post', 'put'))) {
			$currentCalendar = $this->Event->Calendar->findById($calendarId);
			if ($currentCalendar['Calendar']['account_id'] == $currentAccount['Account']['id']) {
				$myEvent = $this->request->data;
				if ($myEvent['Event']['frequency_type_id'] == ''
						&& $myEvent['Event']['date_end']['month'] == '' && $myEvent['Event']['date_end']['day'] == '' && $myEvent['Event']['date_end']['year'] == ''
						&& $myEvent['Event']['hour_end']['hour'] == '' && $myEvent['Event']['hour_end']['min'] == '') {
					$this->Session->setFlash(__('You have to specify end date and end hour if event is not recurring.'), 'flash/error');
				} else {
					$myEvent['Event']['calendar_id'] = $id;
					if ($myEvent['Event']['frequency_type_id'] != '' && $myEvent['Event']['interv'] == '') {
						$myEvent['Event']['interv'] = '1';
					}
					$myEvent['Event']['calendar_id'] = $currentEvent['Event']['calendar_id'];
					if ($this->Event->save($myEvent)) {
						$this->Session->setFlash(__('The event has been saved'), 'flash/success');
						return $this->redirect(array('controller' => 'calendars', 'action' => 'edit', $currentEvent['Event']['calendar_id']));
					} else {
						$this->Session->setFlash(__('The event could not be saved'), 'flash/error');
					}
				}
			} else {
				$this->Session->setFlash(__('You are not allowed to edit this calendar'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Event.' . $this->Event->primaryKey => $id));
			$this->request->data = $this->Event->find('first', $options);
		}
		$calendars = $this->Event->Calendar->find('list', array('conditions' => array('Calendar.account_id' => $currentAccount['Account']['id'])));
		$frequencyTypes = $this->Event->FrequencyType->find('list');
		$weekDays = $this->Event->FrequencyType->WeekDay->find('list');
		$this->set(compact('calendars', 'frequencyTypes', 'weekDays',  'calendarId'));
	}

	/**
	 * admin_delete method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_delete($id = null) {
		$this->Event->id = $id;
		if (!$this->Event->exists()) {
			throw new NotFoundException(__('Invalid event'));
		}
		$currentEvent = $this->Event->findById($id);
		$currentAccount = $this->Session->read('Auth.User');
		if ($currentEvent['Calendar']['account_id'] == $currentAccount['Account']['id']) {
			$this->request->onlyAllow('post', 'delete');
			if ($this->Event->delete()) {
				$this->Session->setFlash(__('The event has been deleted'), 'flash/success');
				return $this->redirect(array('controller' => 'calendars', 'action' => 'edit', $currentEvent['Event']['calendar_id']));
			} else {
				$this->Session->setFlash(__('The event could not be deleted. Please, try again.'), 'flash/error');
			}
		} else {
			$this->Session->setFlash(__('You are not allowed to delete this event'), 'flash/error');
		}
		return $this->redirect(array('controller' => 'calendars', 'action' => 'index'));
	}

	/**
	 * Create an event array. Useful to create a json feed out of a calendar.
	 *
	 * @param string $id a calendar id
	 * @return void
	 */
	public function admin_feed($calId = null) {
		// Adapted from http://duckranger.com/2010/03/fullcalendar-and-cakephp-part-2-create-events-source/
		
		// Get the events corresponding to the time range
		$events = $this->Event->find('all', array(
				'conditions' => array(
						'OR' => array(
								'AND' => array(
									'Event.date_begin <= ?' => array(
											date('Y-m-d', $this->params['url']['end'])
									),
									'Event.frequency_type_id is not null'
								),
								'Event.date_begin BETWEEN ? AND ?' => array(
										date('Y-m-d', $this->params['url']['start']),
										date('Y-m-d', $this->params['url']['end'])
								),
						),
						'Event.calendar_id' => $calId,
				)
		));

		// Create the fullcalendar style array
		$gap = 0;
		$fcEvents = array();
		foreach ($events as $event) {
			$start = new DateTime($event['Event']['date_begin'] . ' ' . $event['Event']['hour_begin']);
			$end = new DateTime($event['Event']['date_end'] . ' ' . $event['Event']['hour_end']);
			
			$eventArray = array(
					'id' => $event['Event']['id'],
					'title' => $event['Event']['name'],
					'start' => $start->getTimestamp(),
					'allDay' => $event['Event']['is_all_day'],
					'end' => $end->getTimestamp(),
					'url' => 'http://' . $_SERVER['HTTP_HOST'] . Configure::read('app_path') . DS . 'admin' . DS . 'events' . DS . 'edit' . DS . $event['Event']['id'],
			);
			
			if ($event['FrequencyType']['id'] == null) {
				array_push($fcEvents, $eventArray);
			} else {
				$interval = is_int($event['Event']['interv']) && $event['Event']['interv'] > 0 ? $event['Event']['interv'] : 1;
				if ($event['FrequencyType']['id'] == 1) {
					$gap = 86400 * $interval; // 24 * 60 * 60
				}
				
				for ($i = $eventArray['start']; $i <= $this->params['url']['end']; $i += $gap) {
					if ($event['FrequencyType']['id'] == 2) {
						$gap = 7 * 86400 * $interval;
					}
					if ($event['FrequencyType']['id'] == 3) {
						$gap = cal_days_in_month(CAL_GREGORIAN, date('m', $i), date('Y', $i)) * 86400 * $interval;
					}
					if ($event['FrequencyType']['id'] == 4) {
						$x = 3;
						$firstXOfMonth = new DateTime($start->format('1-n-Y'));
						$firstXOfMonth = $firstXOfMonth->format('w') + 1;
						$gap = cal_days_in_month(CAL_GREGORIAN, date('m', $i), date('Y', $i)) - $start->format('j') + ($firstXOfMonth > $start->format('w') + 1 ? ($x - 1) * 7 + $start->format('w') + 1 : 7 - $firstXOfMonth + ($x - 1) * 7 + $start->format('w') + 1);
						$gap *= 86400;
					}
					if ($event['FrequencyType']['id'] == 5) {
						$y = date('Y', $i) + $interval;
						$gap = (($y % 4 == 0 && $y % 100 != 0 || $y % 400 == 0) ? 366 : 365) * 86400 * $interval;
					}
					$eventArray['start'] = $i;
					array_push($fcEvents, $eventArray);
				}
			}
		}
		$this->set(compact('fcEvents'));
	}
}

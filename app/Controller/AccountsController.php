<?php
App::uses('AppController', 'Controller');
/**
 * Accounts Controller
 *
 * @property Account $Account
 * @property PaginatorComponent $Paginator
 */
class AccountsController extends AppController {

	public function beforeFilter() {
		$this->Auth->allow('login');
	}

	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array('Paginator');

	/**
	 * admin_index method
	 *
	 * @return void
	 */
	public function admin_index() {
		$this->Account->recursive = 0;
		$sessionAccount = $this->Session->read('Auth.User');
		$this->set('accounts', $this->paginate());
		$this->set('sessionId', $sessionAccount['Account']['id']);
	}

	/**
	 * admin_view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_view($id = null) {
		if (!$this->Account->exists($id)) {
			throw new NotFoundException(__('Invalid account'));
		}
		$options = array('conditions' => array('Account.' . $this->Account->primaryKey => $id));
		$this->set('account', $this->Account->find('first', $options));
	}

	/**
	 * admin_add method
	 *
	 * @return void
	 */
	public function admin_add() {
		if ($this->request->is('post')) {
			if ($this->request->data['Account']['pwd'] == $this->request->data['Account']['pwdConfirm']) {
				$account = $this->Account->findByLogin($this->request->data['Account']['login']);
				if ($account) {
					$this->Session->setFlash(__('A user with this login already exists'), 'flash/error');
				} else {
					$this->Account->create();
					if ($this->Account->save($this->request->data)) {
						$this->Session->setFlash(__('The account has been saved'), 'flash/success');
						$this->redirect(array('action' => 'index'));
					} else {
						$this->Session->setFlash(__('The account could not be saved'), 'flash/error');
					}
				}
			} else {
				$this->Session->setFlash(__('Password and password confirmation are differents'), 'flash/error');
			}
		}
	}

	/**
	 * admin_edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_edit($id = null) {
		$this->Account->id = $id;
		if (!$this->Account->exists($id)) {
			throw new NotFoundException(__('Invalid account'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			// Check password confirmation
			if (empty($this->request->data['Account']['pwd']) || $this->request->data['Account']['pwd'] == $this->request->data['Account']['pwdConfirm']) {
				$dbAccount = $this->Account->findById($id);

				// Check unique login
				if ($dbAccount['Account']['login'] == $this->request->data['Account']['login']) {
					$isValid = false;

					// Check that there is at least one admin left
					if ($dbAccount['Account']['is_admin'] && !$this->request->data['Account']['is_admin']) {
						$adminNb = $this->Account->find('count', array(
								'conditions' => array('Account.is_admin' => true)
						));
						$isValid = $adminNb > 1;
					} else {
						$isValid = true;
					}
					if ($isValid) {
						if (empty($this->request->data['Account']['pwd'])) {
							$this->request->data['Account']['pwd'] = $dbAccount['Account']['pwd'];
						}
						if ($this->Account->save($this->request->data)) {
							$this->Session->setFlash(__('The account has been saved'), 'flash/success');
							$this->redirect(array('action' => 'index'));
						} else {
							$this->Session->setFlash(__('The account could not be saved'), 'flash/error');
						}
					} else {
						$this->Session->setFlash(__('There have to be at least one administrator for the application'), 'flash/error');
					}
				} else {
					$this->Session->setFlash(__('You can\'t change your login'), 'flash/error');
				}
			} else {
				$this->Session->setFlash(__('Password and password confirmation are differents'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Account.' . $this->Account->primaryKey => $id));
			$currentAccount = $this->Account->find('first', $options);
			$currentAccount['Account']['pwd'] = '';
			$this->request->data = $currentAccount;
		}
	}

	/**
	 * admin_delete method
	 *
	 * @throws NotFoundException
	 * @throws MethodNotAllowedException
	 * @param string $id
	 * @return void
	 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Account->id = $id;
		if (!$this->Account->exists()) {
			throw new NotFoundException(__('Invalid account'));
		}
		$currentAccount = $this->Account->findById($id);

		// Always true : a verification have to be done after this
		$this->Account->Calendar->deleteAll(array('Calendar.account_id' => $currentAccount['Account']['id']));

		$calendarNb = $this->Account->Calendar->find('count', array('conditions' => array('Calendar.account_id' => $currentAccount['Account']['id'])));

		if ($calendarNb == 0) {
			if ($this->Account->delete()) {
				$this->Session->setFlash(__('Account deleted'), 'flash/success');
				$sessionAccount = $this->Session->read('Auth.User');
				if ($currentAccount['Account']['id'] == $sessionAccount['Account']['id']) {
					$this->logout();
				}
				$this->redirect(array('action' => 'index'));
			}
			$this->Session->setFlash(__('Account was not deleted'), 'flash/error');
		} else {
			$this->Session->setFlash(__('Impossible to delete calendars associated to this account'), 'flash/error');
		}
		$this->redirect(array('action' => 'index'));
	}

	/**
	 * Log a user
	 * 
	 * @return void
	 */
	public function login() {
		if (!file_exists(APP . DS . 'Config' . DS . 'database.php')) {
			$this->Session->setFlash(__('Database configuration file is not present'), 'flash/error');
		}
		
		if ($this->request->is('post')) {
			$currentAccount = $this->Account->findByLogin($this->request['data']['airCalendar']['username']);
			if($this->Auth->login($currentAccount)) {
				$this->set('authAccount', $this->Auth->user());
				return $this->redirect($this->Auth->redirectUrl());
			} else {
				$this->Session->setFlash(__('Incorrect username or password'), 'flash/error');
			}
		}
	}

	/**
	 * Logout and redirect to the login page
	 * 
	 * @return void
	 */
	public function logout() {
		$this->redirect($this->Auth->logout());
	}

	/**
	 * Allow to control who can access protected pages
	 * 
	 * @see AppController::isAuthorized()
	 * @param Account $account
	 * @return void
	 */
	public function isAuthorized($account) {
		if ($this->action == 'login' || $this->action == 'logout') {
			return true;
		}
		return $account['Account']['is_admin'];
	}
}

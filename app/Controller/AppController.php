<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	public $theme = "Cakestrap";
	public $components = array(
			'Session',
			'Auth' => array(
					'loginAction' => array(
							'controller'   => 'accounts',
							'action'     => 'login'
					),
					'authenticate' => array(
							'Form' => array(
									'passwordHasher' => 'Blowfish'
							)
					),
					'loginRedirect' => array('admin' => true, 'controller' => 'calendars', 'action' => 'admin_index'),
					'logoutRedirect' => array('controller' => 'accounts', 'action' => 'login')
			),
	);

	public function beforeRender() {
		$isLogged = $this->Session->read('Auth.User');
		$this->set('is_logged', $isLogged);
		if ($isLogged) {
			$currentAccount = $this->Session->read('Auth.User');
			$this->set('login', $currentAccount['Account']['login']);
			$this->set('is_admin', $currentAccount['Account']['is_admin']);
		}
	}
	
	public function beforeFilter() {
		if ($this->Session->check('Config.language')) {
			Configure::write('Config.language', $this->Session->read('Config.language'));
		}
	}
}

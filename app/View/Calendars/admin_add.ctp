
<div
	id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">

		<div class="actions">

			<ul class="list-group">
				<li class="list-group-item"><?= $this->Html->link(__('List Calendars'), array('action' => 'index')); ?>
				</li>
			</ul>
			<!-- /.list-group -->

		</div>
		<!-- /.actions -->

	</div>
	<!-- /#sidebar .col-sm-3 -->

	<div id="page-content" class="col-sm-9">

		<h2>
			<?= __('Admin Add Calendar'); ?>
		</h2>

		<div class="calendars form">

			<?= $this->Form->create('Calendar', array('role' => 'form')); ?>

			<fieldset>

				<div class="form-group">
					<?= $this->Form->input('name', array('class' => 'form-control', 'label' => __('Name') . '*')); ?>
				</div>
				<!-- .form-group -->
				<div class="form-group">
					<?= $this->Form->input('category_id', array('class' => 'form-control', 'label' => __('Category') . '*')); ?>
				</div>
				<!-- .form-group -->
				<div class="form-group">
					<?= $this->Form->input('is_activated', array('label' => __('Is activated'))); ?>
				</div>
				<!-- .form-group -->

				<?= $this->Form->submit(__('Submit'), array('class' => 'btn btn-large btn-primary')); ?>

			</fieldset>

			<?= $this->Form->end(); ?>

		</div>
		<!-- /.form -->

	</div>
	<!-- /#page-content .col-sm-9 -->

</div>
<!-- /#page-container .row-fluid -->


<div
	id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">

		<div class="actions">

			<ul class="list-group">
				<li class="list-group-item"><?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Calendar.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Calendar.id'))); ?>
				</li>
				<li class="list-group-item"><?= $this->Html->link(__('New Event'), array('controller' => 'events', 'action' => 'add', $id)); ?>
				</li>
			</ul>
			<!-- /.list-group -->

		</div>
		<!-- /.actions -->

	</div>
	<!-- /#sidebar .col-sm-3 -->

	<div id="page-content" class="col-sm-9">

		<!-- Full Calendar -->
		<div id='calendar'></div>

		<div class="calendars form">

			<?= $this->Form->create('Calendar', array('role' => 'form')); ?>

			<fieldset>

				<div class="form-group">
					<?= $this->Form->input('id', array('class' => 'form-control')); ?>
				</div>
				<!-- .form-group -->
				<div class="form-group">
					<?= $this->Form->input('name', array('class' => 'form-control', 'label' => __('Name') . '*')); ?>
				</div>
				<!-- .form-group -->
				<div class="form-group">
					<?= $this->Form->input('category_id', array('class' => 'form-control', 'label' => __('Category') . '*')); ?>
				</div>
				<!-- .form-group -->
				<div class="form-group">
					<?= $this->Form->input('is_activated', array('label' => __('Is activated'))); ?>
				</div>
				<!-- .form-group -->

				<div class="form-group">
					<label><?= __('Broadcast adress') . ' : '; ?></label>
					<?= $this->Html->link($_SERVER['HTTP_HOST'] . Configure::read('app_path') . DS . 'calendars' . DS . 'export' . DS . $this->Form->value('Calendar.id'), array('controller' => 'calendars', 'action' => 'export', 'admin' => false, 'full_base' => true, $this->Form->value('Calendar.id'))); ?>
				</div>
				<!-- .form-group -->

				<?= $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary')); ?>

			</fieldset>

			<?= $this->Form->end(); ?>

		</div>
		<!-- /.form -->

	</div>
	<!-- /#page-content .col-sm-9 -->

</div>
<!-- /#page-container .row-fluid -->

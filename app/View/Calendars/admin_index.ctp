
<div
	id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">

		<div class="actions">

			<ul class="list-group">
				<li class="list-group-item"><?= $this->Html->link(__('New Calendar'), array('action' => 'add'), array('class' => '')); ?>
				</li>
			</ul>
			<!-- /.list-group -->

		</div>
		<!-- /.actions -->

	</div>
	<!-- /#sidebar .col-sm-3 -->

	<div id="page-content" class="col-sm-9">

		<div class="calendars index">
			<h2>
				<?= __('Calendars'); ?>
			</h2>

			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th><?= $this->Paginator->sort('name'); ?></th>
							<th><?= $this->Paginator->sort('category'); ?></th>
							<th><?= $this->Paginator->sort('activated'); ?></th>
							<th class="actions"><?= __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($calendars as $calendar): ?>
						<tr>
							<td><?= h($calendar['Calendar']['name']); ?>&nbsp;</td>
							<td><?= h($calendar['Category']['name']); ?>&nbsp;</td>
							<td><?= h($calendar['Calendar']['is_activated']) ? __('Yes') : __('No'); ?>&nbsp;</td>
							<td class="actions"><?= $this->Html->link(__('View'), array('action' => 'view', $calendar['Calendar']['id']), array('class' => 'btn btn-default btn-xs')); ?>
								<?= $this->Html->link(__('Edit'), array('action' => 'edit', $calendar['Calendar']['id']), array('class' => 'btn btn-default btn-xs')); ?>
								<?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $calendar['Calendar']['id']), array('class' => 'btn btn-default btn-xs'), __('Are you sure you want to delete %s?', $calendar['Calendar']['name'])); ?>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
			<!-- /.table-responsive -->

			<p>
				<small> <?php
				echo $this->Paginator->counter(array(
						'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
				));
				?>
				</small>
			</p>

			<ul class="pagination">
				<?php
				echo $this->Paginator->prev('< ' . __('Previous'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
				echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
				echo $this->Paginator->next(__('Next') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
				?>
			</ul>
			<!-- /.pagination -->

		</div>
		<!-- /.index -->

	</div>
	<!-- /#page-content .col-sm-9 -->

</div>
<!-- /#page-container .row-fluid -->


<div
	id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">

		<div class="actions">

			<ul class="list-group">
				<li class="list-group-item"><?= $this->Html->link(__('Edit Calendar'), array('action' => 'edit', $calendar['Calendar']['id']), array('class' => '')); ?>
				</li>
				<li class="list-group-item"><?= $this->Form->postLink(__('Delete Calendar'), array('action' => 'delete', $calendar['Calendar']['id']), array('class' => ''), __('Are you sure you want to delete calendar %s?', $calendar['Calendar']['name'])); ?>
				</li>
				<li class="list-group-item"><?= $this->Html->link(__('List Calendars'), array('action' => 'index'), array('class' => '')); ?>
				</li>
				<li class="list-group-item"><?= $this->Html->link(__('New Calendar'), array('action' => 'add'), array('class' => '')); ?>
				</li>
				<li class="list-group-item"><?= $this->Html->link(__('New Event'), array('controller' => 'events', 'action' => 'add'), array('class' => '')); ?>
				</li>

			</ul>
			<!-- /.list-group -->

		</div>
		<!-- /.actions -->

	</div>
	<!-- /#sidebar .span3 -->

	<div id="page-content" class="col-sm-9">

		<div class="calendars view">

			<h2>
				<?= __('Calendar'); ?>
			</h2>

			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<tbody>
						<tr>
							<td><strong><?= __('Name'); ?> </strong></td>
							<td><?= h($calendar['Calendar']['name']); ?> &nbsp;</td>
						</tr>
						<tr>
							<td><strong><?= __('Category'); ?> </strong></td>
							<td><?= $this->Html->link($calendar['Category']['name'], array('controller' => 'categories', 'action' => 'view', $calendar['Category']['id']), array('class' => '')); ?>
								&nbsp;</td>
						</tr>
						<tr>
							<td><strong><?= __('Is Activated'); ?> </strong></td>
							<td><?= h($calendar['Calendar']['is_activated']) ? __('Yes') : __('No'); ?>
								&nbsp;</td>
						</tr>
						<tr>
							<td><strong><?= __('Broadcast adress'); ?> </strong></td>
							<td><?= $this->Html->link($_SERVER['HTTP_HOST'] . DS . basename(dirname(APP)) . DS . 'calendars' . DS . 'export' . DS . $calendar['Calendar']['id'], array('controller' => 'calendars', 'action' => 'export', 'admin' => false, 'full_base' => true, $calendar['Calendar']['id'])); ?>
							</td>
						</tr>
						<!-- .form-group -->
					</tbody>
				</table>
				<!-- /.table table-striped table-bordered -->
			</div>
			<!-- /.table-responsive -->

		</div>
		<!-- /.view -->


		<div class="related">

			<h3>
				<?= __('Related Events'); ?>
			</h3>

			<?php if (!empty($calendar['Event'])): ?>

			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th><?= __('Name'); ?></th>
							<th><?= __('Date Begin'); ?></th>
							<th><?= __('All Day'); ?></th>
							<th><?= __('Date End'); ?></th>
							<th class="actions"><?= __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php
						$i = 0;
						foreach ($calendar['Event'] as $event): ?>
						<tr>
							<td><?= $event['name']; ?></td>
							<td><?= $event['date_begin']; ?></td>
							<td><?= $event['is_all_day'] ? __('Yes') : __('No'); ?></td>
							<td><?= $event['date_end']; ?></td>
							<td class="actions">
								<?= $this->Html->link(__('Edit'), array('controller' => 'events', 'action' => 'edit', $event['id']), array('class' => 'btn btn-default btn-xs')); ?>
								<?= $this->Form->postLink(__('Delete'), array('controller' => 'events', 'action' => 'delete', $event['id']), array('class' => 'btn btn-default btn-xs'), __('Are you sure you want to delete event %s?', $event['name'])); ?>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<!-- /.table table-striped table-bordered -->
			</div>
			<!-- /.table-responsive -->

			<?php endif; ?>


			<div class="actions">
				<?= $this->Html->link('<span class="glyphicon glyphicon-plus"></span> '.__('New Event'), array('controller' => 'events', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>
			</div>
			<!-- /.actions -->

		</div>
		<!-- /.related -->


	</div>
	<!-- /#page-content .span9 -->

</div>
<!-- /#page-container .row-fluid -->

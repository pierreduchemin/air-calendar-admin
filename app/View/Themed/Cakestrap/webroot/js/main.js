$(document).ready(function() {

	// Set a better style for all selects
	$('select').selectpicker();

	// Fullcalendar initalisation
	var url = $(location).attr('pathname').split('/');
	var id = url[url.length - 1];

	$('#calendar').fullCalendar({
		header : {
			left : 'prev,next today',
			center : 'title',
			right : 'month,agendaWeek,agendaDay'
		},
		events : "/air-calendar-admin/admin/events/feed/" + id,
		
		// Comment from here to get fullCalendar in english
		monthNames:['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		monthNamesShort:['janv.','févr.','mars','avr.','mai','juin','juil.','août','sept.','oct.','nov.','déc.'],
		dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
		dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
		titleFormat: {
		    month: 'MMMM yyyy',
		    week: "d[ MMMM][ yyyy]{ - d MMMM yyyy}",
		    day: 'dddd d MMMM yyyy'
		},
		columnFormat: {
		    month: 'ddd',
			week: 'ddd d',
			day: ''
		},
		axisFormat: 'H:mm', 
		timeFormat: {
		    '': 'H:mm', 
		    agenda: 'H:mm{ - H:mm}'
		},
		firstDay:1,
		buttonText: {
		    today: 'aujourd\'hui',
		    day: 'jour',
		    week:'semaine',
		    month:'mois'
		}, 
		header: {
		    left: 'prev,next today',
			center: 'title',
			right: 'month,basicWeek,basicDay'
		},
	});

	// Manage event form
	if ($('#EventIsAllDay').is(':checked')) {
		$('#controls1').hide();
		$('#controls2').hide();
	} else {
		$('#controls1').show();
		$('#controls2').show();
	}

	$('#EventIsAllDay').change(function() {
		if ($('#EventIsAllDay').is(':checked')) {
			$('#controls1').slideUp();
			$('#controls2').slideUp();
		} else {
			$('#controls1').slideDown();
			$('#controls2').slideDown();
		}
	});

	var freq = $('#EventFrequencyTypeId').val();
	if (freq) {
		if (freq == 2) {
			$('#controls4').show();
		} else {
			$('#controls4').hide();
		}
		$('#controls3').show();
	} else {
		$('#controls3').hide();
	}

	$('#EventFrequencyTypeId').change(function() {
		var freq = $('#EventFrequencyTypeId').val();
		if (freq) {
			if (freq == 2) {
				$('#controls4').slideDown();
			} else {
				$('#controls4').slideUp();
			}
			$('#controls3').slideDown();
		} else {
			$('#controls3').slideUp();
		}
	});
});

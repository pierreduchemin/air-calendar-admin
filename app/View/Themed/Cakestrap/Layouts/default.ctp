<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
echo $this->Html->docType('html5'); ?>
<html>
	<head>
		<?= $this->Html->charset(); ?>
		<title>
			<?= __(ucfirst($this->params['controller'])) ?>
		</title>
		<?php
			echo $this->Html->meta('icon');
			
			echo $this->fetch('meta');

			echo $this->Html->css('bootstrap');
			echo $this->Html->css('bootstrap-select.min');
			echo $this->Html->css('fullcalendar');
			echo $this->Html->css('main');

			echo $this->fetch('css');
			
			echo $this->fetch('script');
		?>
	</head>

	<body>

		<div id="main-container">
		
			<div id="header" class="container">
				<?= $this->element('menu/top_menu'); ?>
			</div><!-- /#header .container -->
			
			<div id="content" class="container">
				<?= $this->element('nojs/default'); ?>
				<?= $this->Session->flash(); ?>
				<?= $this->fetch('content'); ?>
			</div><!-- /#content .container -->
			
		</div><!-- /#main-container -->
	<?php
		if (Configure::read('debug') > 0):
			echo '<div class="container">
				<div class="well well-sm">
					<small>'
					. $this->element('sql_dump') .
					'</small>
				</div>
			</div>';
		endif;
		
		// Placing js at the end helps to render pages faster
		echo $this->Html->script('libs/jquery-1.11.0.min');
		echo $this->Html->script('libs/jquery-ui.custom.min');
		echo $this->Html->script('libs/fullcalendar.min');
		echo $this->Html->script('libs/bootstrap.min');
		echo $this->Html->script('libs/bootstrap-select.min');
		echo $this->Html->script('main');
	?>
	</body>

</html>

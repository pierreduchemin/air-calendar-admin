<nav class="navbar navbar-default">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>
		<!-- /.navbar-toggle -->
		<?php if ($this->Session->read('Auth.User')) : ?>
		<?= $this->Html->Link(__('Air Calendar'), array('controller' => 'calendars', 'action' => 'index'), array('class' => 'navbar-brand')); ?>
		<?php else : ?>
		<a class="navbar-brand"><?= __('Air Calendar'); ?></a>
		<?php endif; ?>
	</div>
	<!-- /.navbar-header -->

	<?php if ($is_logged) : ?>
	<div class="collapse navbar-collapse navbar-ex1-collapse">
		<ul class="nav navbar-nav">
			<li
			<?php if ($this->request->params['controller'] == 'calendars') {echo ' class="active"';} ?>>
				<?= $this->Html->link(__('Calendars'), array('controller' => 'calendars', 'action' => 'index', 'admin' => true)); ?>
			</li>
			<li
			<?php if ($this->request->params['controller'] == 'categories') {echo ' class="active"';} ?>>
				<?= $this->Html->link(__('Categories'), array('controller' => 'categories', 'action' => 'index', 'admin' => true)); ?>
			</li>
			<?php if ($is_admin) : ?>
			<li
			<?php if ($this->request->params['controller'] == 'accounts') {echo ' class="active"';} ?>>
				<?= $this->Html->link(__('Accounts'), array('controller' => 'accounts', 'action' => 'index', 'admin' => true)); ?>
			</li>
			<?php endif; ?>
		</ul>
		<ul class="nav navbar-nav pull-right">
			<li><?= $this->Html->link(__('Logout') . ' (' . $login . ')', array('admin' => false, 'controller' => 'accounts', 'action' => 'logout')); ?>
			</li>
		</ul>
		<!-- /.nav navbar-nav -->
	</div>
	<!-- /.navbar-collapse -->
	<?php endif; ?>
</nav>
<!-- /.navbar navbar-default -->

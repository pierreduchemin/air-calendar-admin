<div class="container">

	<?= $this->Form->create('airCalendar', array('class' => 'col-md-4 col-md-offset-4 form-signin')); ?>

	<h2 class="form-signin-heading">
		<?= __('Please sign in'); ?>
	</h2>
	<div class="form-group">
		<?= $this->Form->input('username', array('class' => 'form-control', 'placeholder' => __('Enter username'), 'required' => true, 'autofocus' => true, 'label' => __('Username'))); ?>
	</div>
	<div class="form-group">
		<?= $this->Form->input('password', array('class' => 'form-control', 'placeholder' => __('Password'), 'required' => true, 'label' => __('Password'))); ?>
	</div>

	<?= $this->Form->submit(__('Sign in'), array('class' => 'btn btn-lg btn-primary btn-block')); ?>

	<?= $this->Form->end(); ?>

</div>
<!-- /container -->


<div
	id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">

		<div class="actions">

			<ul class="list-group">
				<li class="list-group-item"><?= $this->Html->link(__('List Accounts'), array('action' => 'index')); ?>
				</li>
			</ul>
			<!-- /.list-group -->

		</div>
		<!-- /.actions -->

	</div>
	<!-- /#sidebar .col-sm-3 -->

	<div id="page-content" class="col-sm-9">

		<h2>
			<?= __('Admin Add Account'); ?>
		</h2>

		<div class="accounts form">

			<?= $this->Form->create('Account', array('role' => 'form')); ?>

			<fieldset>

				<div class="form-group">
					<?= $this->Form->input('login', array('class' => 'form-control', 'label' => __('Login') . '*')); ?>
				</div>
				<!-- .form-group -->
				<div class="form-group">
					<?= $this->Form->input('pwd', array('class' => 'form-control', 'label' => __('Password') . '*', 'type' => 'password')); ?>
				</div>
				<!-- .form-group -->
				<div class="form-group">
					<?= $this->Form->input('pwdConfirm', array('class' => 'form-control', 'label' => __('Password confirmation') . '*', 'type' => 'password')); ?>
				</div>
				<!-- .form-group -->
				<div class="form-group">
					<?= $this->Form->input('is_admin', array('class' => 'form-control', 'label' => __('Is admin'))); ?>
				</div>
				<!-- .form-group -->

				<?= $this->Form->submit(__('Submit'), array('class' => 'btn btn-large btn-primary')); ?>

			</fieldset>

			<?= $this->Form->end(); ?>

		</div>
		<!-- /.form -->

	</div>
	<!-- /#page-content .col-sm-9 -->

</div>
<!-- /#page-container .row-fluid -->

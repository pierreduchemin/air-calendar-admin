
<div
	id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">

		<div class="actions">

			<ul class="list-group">
				<li class="list-group-item"><?= $this->Html->link(__('Edit Account'), array('action' => 'edit', $account['Account']['id']), array('class' => '')); ?>
				</li>
				<li class="list-group-item"><?= $this->Form->postLink(__('Delete Account'), array('action' => 'delete', $account['Account']['id']), array('class' => ''), __('Are you sure you want to delete account %s?', $account['Account']['login'])); ?>
				</li>
				<li class="list-group-item"><?= $this->Html->link(__('List Accounts'), array('action' => 'index'), array('class' => '')); ?>
				</li>
				<li class="list-group-item"><?= $this->Html->link(__('New Account'), array('action' => 'add'), array('class' => '')); ?>
				</li>
				<li class="list-group-item"><?= $this->Html->link(__('New Calendar'), array('controller' => 'calendars', 'action' => 'add'), array('class' => '')); ?>
				</li>

			</ul>
			<!-- /.list-group -->

		</div>
		<!-- /.actions -->

	</div>
	<!-- /#sidebar .span3 -->

	<div id="page-content" class="col-sm-9">

		<div class="accounts view">

			<h2>
				<?php  echo __('Account'); ?>
			</h2>

			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<tbody>
						<tr>
							<td><strong><?= __('Login'); ?> </strong></td>
							<td><?= h($account['Account']['login']); ?> &nbsp;</td>
						</tr>
						<tr>
							<td><strong><?= __('Role'); ?> </strong></td>
							<td><?= h($account['Account']['is_admin']) ? __('Administrator') : __('User'); ?>
								&nbsp;</td>
						</tr>
					</tbody>
				</table>
				<!-- /.table table-striped table-bordered -->
			</div>
			<!-- /.table-responsive -->

		</div>
		<!-- /.view -->


		<div class="related">

			<h3>
				<?= __('Related Calendars'); ?>
			</h3>

			<?php if (!empty($account['Calendar'])): ?>

			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th><?= __('Name'); ?></th>
							<th><?= __('Is Activated'); ?></th>
							<th class="actions"><?= __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php
						$i = 0;
						foreach ($account['Calendar'] as $calendar): ?>
						<tr>
							<td><?= $calendar['name']; ?></td>
							<td><?= $calendar['is_activated'] ? __('Yes') : __('No'); ?></td>
							<td class="actions"><?= $this->Html->link(__('View'), array('controller' => 'calendars', 'action' => 'view', $calendar['id']), array('class' => 'btn btn-default btn-xs')); ?>
								<?= $this->Html->link(__('Edit'), array('controller' => 'calendars', 'action' => 'edit', $calendar['id']), array('class' => 'btn btn-default btn-xs')); ?>
								<?= $this->Form->postLink(__('Delete'), array('controller' => 'calendars', 'action' => 'delete', $calendar['id']), array('class' => 'btn btn-default btn-xs'), __('Are you sure you want to delete calendar %s?', $calendar['name'])); ?>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<!-- /.table table-striped table-bordered -->
			</div>
			<!-- /.table-responsive -->

			<?php endif; ?>


			<div class="actions">
				<?= $this->Html->link('<span class="glyphicon glyphicon-plus"></span> '.__('New Calendar'), array('controller' => 'calendars', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>
			</div>
			<!-- /.actions -->

		</div>
		<!-- /.related -->


	</div>
	<!-- /#page-content .span9 -->

</div>
<!-- /#page-container .row-fluid -->

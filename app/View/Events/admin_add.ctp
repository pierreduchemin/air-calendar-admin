
<div
	id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">

		<div class="actions">

			<ul class="list-group">
				<li class="list-group-item"><?= $this->Html->link(__('List Calendars'), array('controller' => 'calendars', 'action' => 'index')); ?>
				</li>
			</ul>
			<!-- /.list-group -->

		</div>
		<!-- /.actions -->

	</div>
	<!-- /#sidebar .col-sm-3 -->

	<div id="page-content" class="col-sm-9">

		<h2>
			<?= __('Admin Add Event'); ?>
		</h2>

		<div class="events form">

			<?= $this->Form->create('Event', array('role' => 'form')); ?>

			<fieldset>

				<div class="form-group">
					<?= $this->Form->input('name', array('class' => 'form-control', 'label' => __('Name') . '*')); ?>
				</div>
				<!-- .form-group -->
				<div class="form-group">
					<?= $this->Form->input('place', array('class' => 'form-control')); ?>
				</div>
				<!-- .form-group -->
				<div class="form-group">
					<?= $this->Form->input('is_all_day', array('label' => __('All day'))); ?>
				</div>
				<!-- .form-group -->
				<div class="form-group time-input">
					<?= $this->Form->input('date_begin', array('class' => 'form-control', 'label' => __('From') . '*')); ?>
				</div>
				<!-- .form-group -->
				<div id="controls1">
					<div class="form-group time-input">
						<?= $this->Form->input('hour_begin', array('class' => 'form-control', 'label' => __('Begin hour'))); ?>
					</div>
				</div>
				<!-- .form-group -->
				<div class="form-group time-input">
					<?= $this->Form->input('date_end', array('class' => 'form-control', 'label' => __('To'), 'empty' => __('∅'))); ?>
				</div>
				<!-- .form-group -->
				<div id="controls2">
					<div class="form-group time-input">
						<?= $this->Form->input('hour_end', array('class' => 'form-control', 'label' => __('End hour'), 'empty' => __('∅'))); ?>
					</div>
				</div>
				<!-- .form-group -->
				<div class="form-group">
					<?= $this->Form->input('summary', array('class' => 'form-control')); ?>
				</div>
				<!-- .form-group -->
				<div class="form-group">
					<?= $this->Form->input('frequency_type_id', array('class' => 'form-control', 'empty' => __('one-time event'), 'label' => __('Frequency type'))); ?>
				</div>
				<!-- .form-group -->
				
				<div id="controls3">
					<div class="form-group">
						<?= $this->Form->input('interv', array('class' => 'form-control', 'label' => __('Interval'))); ?>
					</div>
					<!-- .form-group -->
					
					<div id="controls4">
						<div class="form-group">
							<?= $this->Form->input('week_day_id', array('type' => 'select', 'multiple'=>'checkbox', 'label' => __('Days'))); ?>
						</div>
						<!-- .form-group -->
					</div>
				</div>

				<?= $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary')); ?>

			</fieldset>

			<?= $this->Form->end(); ?>

		</div>
		<!-- /.form -->

	</div>
	<!-- /#page-content .col-sm-9 -->

</div>
<!-- /#page-container .row-fluid -->


<div
	id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">

		<div class="actions">

			<ul class="list-group">
				<li class="list-group-item"><?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Category.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Category.id'))); ?>
				</li>
				<li class="list-group-item"><?= $this->Html->link(__('New Calendar'), array('controller' => 'calendars', 'action' => 'add')); ?>
				</li>
			</ul>
			<!-- /.list-group -->

		</div>
		<!-- /.actions -->

	</div>
	<!-- /#sidebar .col-sm-3 -->

	<div id="page-content" class="col-sm-9">

		<h2>
			<?= __('Admin Edit Category'); ?>
		</h2>

		<div class="categories form">

			<?= $this->Form->create('Category', array('role' => 'form')); ?>

			<fieldset>
				<div class="form-group">
					<?= $this->Form->input('name', array('class' => 'form-control', 'label' => __('Name') . '*')); ?>
				</div>
				<!-- .form-group -->

				<?= $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary')); ?>

			</fieldset>

			<?= $this->Form->end(); ?>

		</div>
		<!-- /.form -->

	</div>
	<!-- /#page-content .col-sm-9 -->

</div>
<!-- /#page-container .row-fluid -->

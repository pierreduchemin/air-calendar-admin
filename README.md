#CakePHP install problems
##General notes :
* Nerver use apache's userdir mod
* Always check folder permissions after you installed anything
* php5-json is required : sudo apt-get install php5-json

##Timezone problem :
* Uncomment "date_default_timezone_set('UTC');" in app/Config/core.php and replace UTC if needed. See : http://www.php.net/manual/en/timezones.php

##Security problems :
* Change "Security.salt" in app/Config/core.php
* Change "Security.cipherSeed" in app/Config/core.php

##Cache problems :
* sudo mkdir app/tmp
* sudo chmod 777 app/tmp/ -R

##URL rewriting problems :
* sudo vi /etc/apache2/apache2.conf : "AllowOverride" of "<Directory />" and of "<Directory /var/www>" to "All"
* sudo a2enmod rewrite
* sudo service apache2 restart

##Cake bake problems :
* sudo apt-get install php5-cli
* sudo chmod +x app/Console/cake
* sudo chmod 777 app/tmp/ -R


#Application install instructions
* Set rights for export folder : sudo chown www-data app/webroot/ical/ -R && sudo chmod 777 app/webroot/ical/ -R


#Postgresql install

##Login as postgresql :
sudo -i -u postgres

##Create a user :
CREATE USER <nom_utilisateur>;
ALTER ROLE <nom_utilisateur> WITH CREATEDB;

##Create a first db :
CREATE DATABASE <nom_base_de_donnee> OWNER <nom_utilisateur>;

##Set a password :
ALTER USER <nom_utilisateur> WITH ENCRYPTED PASSWORD 'mon_mot_de_passe';

##Connect to db :
psql nom_base_de_donnee


#Requirements
* a CakePHP capable server
* JavaScript activated
* a modern browser (IE 8+)


#Credits
Some icons by [Yusuke Kamiyamane](http://p.yusukekamiyamane.com/). Licensed under a [Creative Commons Attribution 3.0 License](http://creativecommons.org/licenses/by/3.0/).
Calendar element by [Fullcalendar](http://arshaw.com/fullcalendar/). Licensed under a MIT Licence.
